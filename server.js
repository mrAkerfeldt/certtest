//modules
var express = require('express'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    https = require('https');
    http = require('http');


//We get the ssl cert and key, and then bind them to a variable

var key = fs.readFileSync('certificate/key.key');
var cert = fs.readFileSync('certificate/cert.cert');

var options = {
    key: key,
    cert: cert,
};

//setting expressl
var app = express();
app.use(express.static(__dirname + '/public'));
ports = 8080;
port = 80;


//The use of the bodyParser constructor (app.use(bodyParser());) has been deprecated
//Now is a middleware, so you have to call the methods separately:
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

//Allow CROSS-ORIGIN
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', "POST, GET, PUT, DELETE, OPTIONS, HELP");
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

https.createServer(options,function (req, res){
    res.writeHead(200);
    res.end('Express server listening on localhost:' + ports + ' over HTTPS');
}).listen(ports);

//We can create an http server to redirect every connection to https, as we would do modifying a .htaccess file when using cPanel

http.createServer(function (req, res) {

    //We can redirect http connections to https with the following code
    //res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url});
    //res.end();

    //As we are working locally we will just write a message as before
    res.writeHead(200);
    res.end('Express server listening on localhost:' + port + ' over HTTP');

}).listen(port);
